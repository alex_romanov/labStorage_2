package taskmanager;

import java.io.Serializable;
import java.util.Date;
//Описание класса с гетерами и сетерами
public class Task  implements Serializable{
    private Date date = null;
        private String time = null;
        private String name = null;
        private String description = null;

        public Task() {
        }

        public Task(Date date,String time, String name, String description) {
            this.date = date;
            this.time = time;
            this.name = name;
            this.description = description;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }

