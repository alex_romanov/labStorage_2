package taskmanager;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Desearizator {
    public static ArrayList fromXML(File fileSrc) throws Exception {
            //Десериализирует из XML файла в ArrayList при помощи XMLDecoder
            XMLDecoder decode = null;
            ArrayList arrayList = null;
            try {
                decode = new XMLDecoder(createFileStream(fileSrc));
                arrayList = (ArrayList) decode.readObject();
            } catch (Exception e) {
                arrayList = null;
                throw new Exception();
            } finally {
                decode.close();
                return arrayList;
            }
        }

        private static BufferedInputStream createFileStream(File file) throws FileNotFoundException {
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            BufferedInputStream bis = new BufferedInputStream(fis);
            return bis;
        }
    }


