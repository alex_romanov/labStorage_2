package taskmanager;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class Serealizator {
    public static void toXML(File fileDest, ArrayList listDest) throws FileNotFoundException {
        //Сереализирует ArrayList в XML файл при помощи XMLEncoder
        XMLEncoder encode = null;
        try {
            encode = new XMLEncoder(createFileStream(fileDest));
            encode.writeObject(listDest);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        } finally {
            encode.close();
        }
    }

    private static BufferedOutputStream createFileStream(File file) throws FileNotFoundException {
        FileOutputStream fos = new FileOutputStream(file.getAbsolutePath());
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        return bos;
    }
}

