package taskmanager;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Runnable {
    @Override
    public void run() {
        System.out.println("Сервер начал работу");
        ArrayList<Object> clients = new ArrayList<>();
        TaskController controller = new TaskController();
     ArrayList<Task> list = new ArrayList<>();
        ObjectOutputStream objectOutputStream;
        ObjectInputStream objectInputStream;
        try {
            controller.load();
            list.addAll(controller.load());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (ServerSocket server = new ServerSocket(3351)) {
            Socket client = server.accept();
        objectOutputStream = new ObjectOutputStream(client.getOutputStream());
        objectInputStream = new ObjectInputStream(client.getInputStream());
            System.out.print("Соединение установлено.");
            clients.add(client);
            System.out.println("Количество клиентов " + clients.size());
            while (!client.isClosed()) {
                System.out.println("Сервер принимает сообщение от клиента");
                String entry = (String) objectInputStream.readObject();
                System.out.println("Клиент ввел команду (" + entry + ") ");
                switch (entry) {
                    case ("Вывод всех задач"): {
                        objectOutputStream.writeObject(list);
                        break;
                    }
                    case ("Создать задачу"): {
                        Task task = (Task) objectInputStream.readObject();
                        controller.save(task);
                        System.out.println("Задача записана");
                        break;
                    }
                    case ("Удалить задачу"): {
                        int index = objectInputStream.readInt();
                        controller.removeTask(index);
                        controller.save();
                        System.out.println("Задача удалена. Введите команду");
                        break;
                    }
                    case ("Изменить задачу"): {
                        int index = objectInputStream.readInt();
                        Task task = (Task) objectInputStream.readObject();
                        controller.editTask(index, task);
                        controller.save();
                        System.out.println("Задача изменена");
                        break;
                    }
                    case ("Отследить задачи"): {
                        objectOutputStream.writeObject(list);
                        break;
                    }
                }
                if (entry.equalsIgnoreCase("Закрыть")) {
                    System.out.println("Клиент разорвал соединение");
                    break;
                }

                objectOutputStream.flush();
            }
        }
            catch (EOFException e)
            {
                System.out.println("ERROR "+ e.getMessage());
            }
         catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    }



//  стартуем сервер на порту 3345








