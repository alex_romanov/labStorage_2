package taskmanager;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public  class TaskController{
    private File file;
    private ArrayList<Task> taskList = new ArrayList<>();

    public ArrayList<Task> load() throws Exception {
        file = new File("taski.xml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ArrayList<Task> tTaskList;
        try {
            tTaskList = Desearizator.fromXML(file);
            if (tTaskList != null) {
                this.taskList = tTaskList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return taskList;
    }
    public void save() throws FileNotFoundException {
        file = new File("taski.xml");
        System.out.println(file.getAbsolutePath());
        if (file != null) {
            try {
                Serealizator.toXML(file, taskList);
            } catch (FileNotFoundException e) {
                throw new FileNotFoundException();
            }
        } else {
            throw new FileNotFoundException();
        }
    }
    public void save(Task task) throws FileNotFoundException {
        file = new File("taski.xml");
        System.out.println(file.getAbsolutePath());
        taskList.add(task);
        if (file != null) {
            try {
                Serealizator.toXML(file, taskList);
            } catch (FileNotFoundException e) {
                throw new FileNotFoundException();
            }
        } else {
            throw new FileNotFoundException();
        }
    }

    public Task newTask() {
        Task task = new Task();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите дату в формате год/месяц/день Часы:Минуты");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String dat = scanner.nextLine();
        try {
            task.setDate(simpleDateFormat.parse(dat));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Введите время которое планируете потратить на выполнение задачи");
        task.setTime(scanner.nextLine());
        System.out.println("Введите имя задачи");
        task.setName(scanner.nextLine());
        System.out.println("Введите описание задачи");
        task.setDescription(scanner.nextLine());
        taskList.add(task);
        return task;
    }

    public ArrayList<Task> removeTask(int index) {
        taskList.remove(index-1);
        try {
            save();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return taskList;
    }

    public ArrayList<Task> editTask(int index, Task task) {
        taskList.set(index-1, task);
        return taskList;
    }

    public void date(Date date, ArrayList<Task> list) {
        for (int index = 0 ; index < list.size(); index++) {
            if (list.get(index).getDate().getYear() < date.getYear()
            | list.get(index).getDate().getYear() == date.getYear()
            & list.get(index).getDate().getMonth() < date.getMonth()
                    | list.get(index).getDate().getDay() < date.getDay()
            | list.get(index).getDate().getDay() == date.getDay()
                    & list.get(index).getDate().getHours() < date.getHours()
                    | list.get(index).getDate().getHours() == date.getHours()
                    & list.get(index).getDate().getMinutes() < date.getMinutes())
            {
                System.out.println("Срок выполнение задачи  " + list.get(index).getName() +  " истек");
            }
           else if (list.get(index).getDate().getYear() == date.getYear()
                    & list.get(index).getDate().getMonth() == date.getMonth()
                    & list.get(index).getDate().getDay() == date.getDay()
                    & list.get(index).getDate().getHours() == date.getHours()) {
                long m = list.get(index).getDate().getMinutes() - date.getMinutes();
                System.out.println("До задачи " + list.get(index).getName() + " осталось " + m + " минут");
                if (list.get(index).getDate().getMinutes() - date.getMinutes() == 5) {
                    System.out.println("Отложить задачу на опреденное вами количество  минут?");
                    System.out.println(" 1) Да - откладывает задачу на определенное вами количество минут");
                    System.out.println(" 2) Нет - задача готовится приступить к выполнению");
                    Scanner scanner = new Scanner(System.in);
                    switch (scanner.nextLine()) {
                        case ("Да"): {
                            try {
                                System.out.println("Введите количество минут");
                                int min = scanner.nextInt();
                                System.out.println("Задача отложена на " + min + " минут");
                                Thread.sleep(min * 60000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case ("Нет"): {
                            break;
                        }
                    }
                }
            }
        }
    }
}


