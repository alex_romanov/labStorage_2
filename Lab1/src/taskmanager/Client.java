package taskmanager;


import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;


public class Client implements Runnable {
    Socket socket;
    TaskController controller;
    BufferedReader br;
    Date date = new Date();
Client(String host, int port) throws IOException {
    socket = new Socket(host,port);
    controller = new TaskController();
    br = new BufferedReader(new InputStreamReader(System.in));
}
    @Override
    public void run() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Клиент подключился");
            System.out.println();
            System.out.println("Введите команду");
            System.out.println(" 1) Отследить задачи - Отслеживает какие задачи вы пропустили либо какие задачи вам предстоит сделать");
            System.out.println(" 2) Закрыть - завершение работы");
            System.out.println(" 3) Создать задачу - создает задачу ");
            System.out.println(" 4) Удалить задачу - удаляет раннее созданную задачу");
            System.out.println(" 6) Изменить задачу - изменяет раннее созданную задачу");
            System.out.println(" 7) Вывод всех задач - выводит все задачи, записанные на сервере");
            while (!socket.isOutputShutdown()) {
                System.out.println("Клиент начал работу с сервером");
                String clientCommand = br.readLine();
                System.out.println("Клиент отправил команду (" + clientCommand + ") на сервер.");
                objectOutputStream.writeObject(clientCommand);
                System.out.println();
                switch (clientCommand) {
                    case ("Вывод всех задач"): {
                        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                        ArrayList<Task> taskList = (ArrayList<Task>) objectInputStream.readObject();
                            System.out.println("Имеющиеся задачи");
                            for (int i = 0; i < taskList.size(); i++) {
                                System.out.println("Номер: " + (i + 1)
                                        + " Дата: " + taskList.get(i).getDate()
                                        + " Time: " + taskList.get(i).getTime()
                                        + " Имя: " + taskList.get(i).getName()
                                        + " Описание: " + taskList.get(i).getDescription());
                        }
                        objectInputStream.close();
                        break;
                    }

                    case ("Создать задачу"): {
                        objectOutputStream.writeObject(controller.newTask());
                        break;
                    }
                    case ("Удалить задачу"): {
                        System.out.println("Введите номер задачи. От 1 до n");
                        objectOutputStream.write(Integer.parseInt(br.readLine()));
                        System.out.println("Задача удалена. Введите команду");
                        break;
                    }
                    case ("Изменить задачу"): {
                        System.out.println("Введите номер задачи. От 1 до n");
                        objectOutputStream.write(Integer.parseInt(br.readLine()));
                        System.out.println("Введите новую задачу");
                        objectOutputStream.writeObject(controller.newTask());
                        System.out.println("Задача изменена. Введите команду");
                        break;
                    }
                    case ("Отследить задачи"): {
                        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                        ArrayList<Task> taskList = (ArrayList<Task>) objectInputStream.readObject();
                        controller.date(date, taskList);
                        objectInputStream.close();
                        break;
                    }
                }
                if (clientCommand.equalsIgnoreCase("Закрыть")) {
                    System.out.println("Клиент разорвал соединение");
                    break;
                }
                objectOutputStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}

   /* public static void main(String[] args) throws InterruptedException {
            TaskController controller = new TaskController();
            TaskNotification taskNotification = new TaskNotification();
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            Date date = new Date();
            System.out.println("Клиент подключился к серверу.");
            System.out.println();
            System.out.println("Введите команду");
            System.out.println(" 1) Отследить задачи - Отслеживает какие задачи вы пропустили либо какие задачи вам предстоит сделать");
            System.out.println(" 2) Закрыть - завершение работы");
            System.out.println(" 3) Создать задачу - создает задачу ");
            System.out.println(" 4) Удалить задачу - удаляет раннее созданную задачу");
            System.out.println(" 6) Изменить задачу - изменяет раннее созданную задачу");
            System.out.println(" 7) Вывод всех задач - выводит все задачи, записанные на сервере");
            try (Socket socket = new Socket("localhost", 3349)) {

                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                while (!socket.isOutputShutdown()) {
                    System.out.println("Клиент начал работу с сервером");
                    String clientCommand = br.readLine();
                    System.out.println("Клиент отправил команду (" + clientCommand + ") на сервер.");
                    objectOutputStream.writeObject(clientCommand);
                    objectOutputStream.flush();
                    System.out.println();
                    switch (clientCommand) {
                        case ("Вывод всех задач"): {
                            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                            ArrayList<Task> taskList = (ArrayList<Task>) objectInputStream.readObject();
                            System.out.println("Имеющиеся задачи");
                            for (int i = 0; i < taskList.size(); i++) {
                                System.out.println("Номер: " + (i + 1)
                                        + " Дата: " + taskList.get(i).getDate()
                                        + " Time: " + taskList.get(i).getTime()
                                        + " Имя: " + taskList.get(i).getName()
                                        + " Описание: " + taskList.get(i).getDescription());
                            }
                            objectInputStream.close();
                            break;
                        }

                        case ("Создать задачу"): {
                            objectOutputStream.writeObject(controller.newTask());
                            break;
                        }
                        case ("Удалить задачу"): {
                            System.out.println("Введите номер задачи. От 1 до n");
                            objectOutputStream.write(Integer.parseInt(br.readLine()));
                            System.out.println("Задача удалена. Введите команду");
                            break;
                        }
                        case ("Изменить задачу"): {
                            System.out.println("Введите номер задачи. От 1 до n");
                            objectOutputStream.write(Integer.parseInt(br.readLine()));
                            System.out.println("Введите новую задачу");
                            objectOutputStream.writeObject(controller.newTask());
                            System.out.println("Задача изменена. Введите команду");
                            break;
                        }
                        case ("Отследить задачи"): {
                            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                            ArrayList<Task> taskList = (ArrayList<Task>) objectInputStream.readObject();
                            controller.date(date, taskList);
                            objectInputStream.close();
                            break;
                        }
                    }
                    if (clientCommand.equalsIgnoreCase("Закрыть")) {
                        System.out.println("Клиент разорвал соединение");
                        break;
                    }
                }
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }



    @Override
    public void run() {

    }
}
*/