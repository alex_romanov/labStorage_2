package taskmanager;

public class TaskManagerServer {
    public static void main(String[] args) throws Exception {
        Server server = new Server();
        server.run();

    }
}






/*
    private static void command() {
        TaskController controller = new TaskController();
        try {
            controller.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Scanner scanner = new Scanner(System.in);
            System.out.println("Введите команду:");
        System.out.println(" 1) Создать задачу - создает задачу ");
        System.out.println(" 2) Удалить задачу - удаляет раннее созданную задачу");
        System.out.println(" 3) Найти задачу - выводится на экран раннее созданная задача ");
        System.out.println(" 4) Изменить задачу - изменяет раннее созданную задачу");
        System.out.println(" 5) Закрыть - завершает работу");
        boolean t = true;
            while (t) {
                switch (scanner.nextLine()) {
                    case ("Создать задачу"): {
                        controller.newTask();
                        System.out.println("Задача создана. Введите команду");
                        break;
                    }
                    case ("Удалить задачу"): {
                        System.out.println("Введите номер задачи. От 1 до n");
                        int i = scanner.nextInt();
                        controller.removeTask(i);
                        System.out.println("Задача удалена. Введите команду");
                        break;
                    }
                    case ("Найти задачу"): {
                        System.out.println("Введите номер задачи. От 1 до n");
                        int i = scanner.nextInt();
                        controller.getTask(i);
                        System.out.println("Задача найдена. Введите команду");
                        break;
                    }
                    case ("Изменить задачу"): {
                        System.out.println("Введите номер задачи. От 1 до n");
                        int i = scanner.nextInt();
                        System.out.println("Введите новую задачу");
                        controller.editTask(i-1, controller.newTask());
                        System.out.println("Задача изменена. Введите команду");
                        break;
                    }
                    case ("Закрыть"): {
                        t= false;
                        System.out.println("Работа окончена");
                        break;
                    }
                }
            }
}
        }

*/
